#!/bin/sh

echo "INSTALLING BREW.."
#install homebrew script using curl
curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh

echo "INSTALLING BREW CASK.."
brew tap homebrew/cask

allApps=("google-chrome" "visual-studio-code" "sublime-text" "graphiql" "bitwarden" "forticlient" "nvm" "iterm2")

echo "INSTALLING APPS.."

for i in ${allApps[@]}; do
echo "Do you wish to install $i"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) 
			echo "BREW INSTALLING $i"
			if [[ "$i" == "nvm" ]]
			then
				echo "NVM INSTALL"
				brew install nvm
			else
				echo "CASK INSTALL"
				brew install --cask $i
			fi
			break;;
			# continue;;
        No ) 
			# want to loop back not exit
			echo "ONTO APP $i"
			exit;;
    esac
done
done

